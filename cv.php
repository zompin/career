<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/phpmailer/PHPMailerAutoload.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/mailerconfig.php';

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$mode = $_POST['mode'];

		switch ($mode) {
			case 'form':
				form($_POST);
			break;
			case 'cv':
				cv();
			break;
		}
	}

	function form($data) {
		GLOBAL $mail;
		$html = '';

		if (count($_FILES)) {
			$file = $_FILES['photo'];
			if ($file && $file['error'] === 0) {
				$name = $file['name'];
				$tmpName = $file['tmp_name'];
				$type = $file['type'];
				$fp = fopen($tmpName, "r");
				$fcontent = base64_encode(fread($fp, filesize($tmpName)));
				$imageContent = "<img src=\"data:" . $type . ";base64," . $fcontent . "\" style='width: 100%; height: auto;'>";
				$html .= '<tr><td colspan="2">' . $imageContent . '</td></tr>';
				fclose($fp);
			}
		}

		$personal = json_decode($data['personal']);
		$education = json_decode($data['education']);
		$experience = json_decode($data['experience']);
		$otherInfo = json_decode($data['otherInfo']);

		$html .= getPersonalInfo($personal);
		$html .= '<tr><td colsapn="2">Образование</td></tr>';
		$html .= '<tr><td colsapn="2">' . getEducation($education) . '</td></tr>';
		$html .= '<tr><td colsapn="2">Опыт</td></tr>';

		if ($personal->haveExperience) {
			$html .= '<tr><td colsapn="2">' . getExperienceInfo($experience) . '</td></tr>';
		} else {
			$html .= '<tr><td colsapn="2">Не указан</td></tr>';
		}

		$html .= '<tr><td colsapn="2">Дополнительная информация</td></tr>';
		$html .= getOtherOnfo($otherInfo);
		$html = '<table>' . $html . '</table>';
		$mail->Body = $html;
		
		sendMail();
	}

	function cv() {
		GLOBAL $mail;
		$mail->Body = "Резюме с сайта";
		$file = $_FILES['cv'];
		$error = $file['error'];

		if ($error === 0) {
			$name = $file['name'];
			$tmpName = $file['tmp_name'];
			$type = $file['type'];
			$mail->addAttachment($tmpName, $name);

			sendMail();
		}
	}

	function sendMail() {
		GLOBAL $mail;
		if (!$mail->send()) {
			echo false;
		} else {
			echo true;
		}
	}

	function getPersonalInfo($personal) {
		$html = '
			<tr>
				<td colspan="2">Личные данные</td>
			</tr>
			<tr>
				<td>Фамилия:</td>
				<td>' . $personal->sname . '</td>
			</tr>
			<tr>
				<td>Имя:</td>
				<td>' . $personal->name . '</td>
			</tr>
			<tr>
				<td>Дата рождения:</td>
				<td>
					<span>' . $personal->dayOfBirth . '</span>
					<span>' . convertMonth($personal->monthOfBirth) . '</span>
					<span>' . $personal->yearOfBirth . '</span>
				</td>
			</tr>
			<tr>
				<td>Пол:</td>
				<td>' . convertSex($personal->sex) . '</td>
			</tr>
			<tr>
				<td>Место проживания:</td>
				<td>' . $personal->living . '</td>
			</tr>
			<tr>
				<td>Гражданство: </td>
				<td>' . $personal->citizen . '</td>
			</tr>
			<tr>
				<td>E-mail:</td>
				<td>' . $personal->email . '</td>
			</tr>
			<tr>
				<td>Телефон:</td>
				<td>' . $personal->phone . '</td>
			</tr>
			<tr>
				<td>Цель:</td>
				<td>' . $personal->aim . '</td>
			</tr>
		';

		return $html;
	}

	function getEducation($education) {
		$html = '';

		foreach ($education as $v) {
			$html .= '
				<tr>
					<td>Начало обучение:</td>
					<td>' . $v->startYear . '</td>
				</tr>
				<tr>
					<td>Окончание обучения:</td>
					<td>' . $v->endYear . '</td>
				</tr>
				<tr>
					<td>Название учебного заведения:</td>
					<td>' . $v->school . '</td>
				</tr>
				<tr>
					<td>Спциальность:</td>
					<td>' . $v->speciality . '</td>
				</tr>
				<tr>
					<td>Факультет:</td>
					<td>' . $v->department . '</td>
				</tr>
			';
		}

		return '<table>' . $html . '</table>';
	}

	function getExperienceInfo($experience) {
		$html = '';

		foreach ($experience as $v) {
			$html .= '
				<tr>
					<td>Начало работы:</td>
					<td><span>' . convertMonth($v->startMonth) . '</span> <span>' . $v->startYear . '</span></td>
				</tr>
				<tr>
					<td>Завершение работы:</td>
					<td><span>' . convertMonth($v->endMonth) . '</span> <span>' . $v->endYear . '</td>
				</tr>
				<tr>
					<td>Название компании:</td>
					<td>' . $v->companyName . '</td>
				</tr>
				<tr>
					<td>Обязанности:</td>
					<td>' . $v->duties . '</td>
				</tr>
				<tr>
					<td>Проекты:</td>
					<td>' . $v->projects . '</td>
				</tr>
			';
		}

		return '' . $html . '</table>';
	}

	function getOtherOnfo($other) {
		$html = '
			<tr>
				<td>Дополнительное образование:</td>
				<td>' . $other->addEducation . '</td>
			</tr>
			<tr>
				<td>Знание языка:</td>
				<td>' . $other->languages . '</td>
			</tr>
			<tr>
				<td>Компьютерная грамотность:</td>
				<td>' . $other->compHabits . '</td>
			</tr>
			<tr>
				<td>Дополнительные сведения:</td>
				<td>' . $other->addInfo . '</td>
			</tr>
			<tr>
				<td>Личные качества:</td>
				<td>' . $other->personalQualities . '</td>
			</tr>
			<tr>
				<td>Ожидаемый уровень дохода</td>
				<td>' . $other->payment . '</td>
			</tr>
		';

		return $html;
	}

	function convertMonth($num) {
		$months = array(
				'Январь',
				'Февраль',
				'Март',
				'Апрель',
				'Май',
				'Июнь',
				'Июль',
				'Август',
				'Сентябрь',
				'Октябрь',
				'Ноябрь',
				'Декабрь'
			);

		return $months[$num];
	}

	function convertSex($sex) {
		switch ($sex) {
			case 'male':
				$sex = 'Мужской';
			break;
			case 'female':
				$sex = 'Женский';
			break;
			default:
				$sex = 'Ошибка';
		}

		return $sex;
	}
?>