<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/phpmailer/PHPMailerAutoload.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/mailerconfig.php';
	$html = '';

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$data = $_POST;

		$html .= '
			<tr>
				<td colspan="2">Заказ обратного звонка</td>
			</tr>
			<tr>
				<td>ФИО:</td>
				<td>' . $data['fname'] . ' ' . $data['name'] . '</td>
			</tr>
			<tr>
				<td>E-mail:</td>
				<td>' . $data['email'] . '</td>
			</tr>
			<tr>
				<td>Телефон:</td>
				<td>' . $data['phone'] . '</td>
			</tr>
			<tr>
				<td>Сообщение:</td>
				<td>' . $data['message'] . '</td>
			</tr>
			';
		$html = '<table>' . $html . '</table>';
		$mail->Body = $html;

		if (!$mail->send()) {
			echo false;
		} else {
			echo true;
		}

	}
?>