var gulp = require('gulp'),
	less = require('gulp-less')
	bs = require('browser-sync').create();

gulp.task('default', ['bs', 'less'], function() {
	gulp.watch('less/*.less', ['less']);
});

gulp.task('bs', function() {
	bs.init({
		proxy: 'career',
		//server: './',
		notify: false,
		//files: ['*.html', '*.php', '*.css', 'js/*.js']
		files: ['*.html', '*.css', 'js/*.js']
	});
});

gulp.task('less', function() {
	return gulp.src('less/*.less').pipe(less()).pipe(gulp.dest('less/../'));
});