ymaps.ready(function() {		
	var map = new ymaps.Map("map", {
		center: [42.304476, 69.631602], 
		zoom: 18,
		controls: []
	});

	var template = ymaps.templateLayoutFactory.createClass('<div id=\"map__mark\">career.kz</div>');
	var placeMark = new ymaps.Placemark(
			map.getCenter(), {}, {
				iconLayout: template
			}
		);
	map.geoObjects.add(placeMark);
});

(function($) {

	$(window).scroll(showHomeButton);
	$(window).on('touchend', showHomeButton);

	function showHomeButton() {
		if ($(document).scrollTop() > 400) {
			if ($('.home-button').is(':hidden')) {
				$('.home-button').show();
			}
		} else {
			if ($('.home-button').is(':visible')) {
				$('.home-button').hide();
			}
		}
	}

	$('.menu-button').click(function() {
		
		if ($('.main-menu').is(':hidden')) {
			$('.main-menu').stop().fadeIn();
		} else {
			$('.main-menu').stop().fadeOut(function() {
				$(this).removeAttr('style');
			});
		}
	});

	$('.home-button').click(function() {
		$('html, body').animate({
			scrollTop: 0
		});
	});

	$.ajax({
		url: '/year.php',
		dataType: 'json',
		success: function(year) {
			year = 2016;
			setOptions('.day', 1, 31);
			setOptions('.month');
			setOptions('.applicant__year-of-birth', 1950, year - 18, true);
			setOptions('.expirience__start-year', 1950, year, true);
			setOptions('.expirience__end-year', 1950, year, true);
			setOptions('.education__start-year', 1950, year, true);
			setOptions('.education__end-year', 1950, year, true);
		}
	});

	$('.mission__text-button').click(function() {
		$('.mission__popup').fadeToggle();
	});

	$('.mission__popup-close').click(function() {
		$('.mission__popup').fadeToggle();
	});

	$('.order__button').click(function() {
		$('.applicant').slideDown({queue: false});
		$('html, body').animate({
			scrollTop: $('.applicant').offset().top
		});
	});

	$('.applicant__close').click(function() {
		$('.applicant').slideToggle();
	});

	// Заполняет select'ы
	function setOptions(el, startYear, endYear, rev) {
		var months = [
			{name: 'Январь', value: 0},
			{name: 'Февраль', value: 1},
			{name: 'Март', value: 2},
			{name: 'Апрель', value: 3},
			{name: 'Май', value: 4},
			{name: 'Июнь', value: 5},
			{name: 'Июль', value: 6},
			{name: 'Август', value: 7},
			{name: 'Сентябрь', value: 8},
			{name: 'Октябрь', value: 9},
			{name: 'Ноябрь', value: 10},
			{name: 'Декабрь', value: 11}
		];

		$(el).each(function(i, e) {
			var j;
			$(e).html(function() {
				var select = '<option class="applicant__option applicant__option_empty"></option>';
				if (!startYear && !endYear) {
					for (i = 0; i < months.length; i++) {
						select += '<option class="applicant__option" value="' + months[i].value + '">' + months[i].name + '</option>';
					}
				} else {
					if (rev) {
						j = endYear
						while (j >= startYear) {
							select += '<option class="applicant__option" value="' + j + '">' + j + '</option>';
							j--
						}
					} else {
						j = startYear;
						while (j <= endYear) {
							select += '<option class="applicant__option" value="' + j + '">' + j + '</option>';
							j++;
						}
					}
				}

				return select;
			});
		});
	}

	$('.main-menu__item').click(function() {
		var division = $(this).attr('href').substr(1);
		if (division) {
			division = $('.' + division)
			if (division.length) {
				if ($('.main-menu').attr('style')) {
					$('.main-menu').fadeOut(function() {
						$('html, body').animate({
							scrollTop: division.offset().top
						});
						$('.main-menu').removeAttr('style');
					});
				} else {
					$('html, body').animate({
						scrollTop: division.offset().top
					});
				}
			}
		}
	});

	$('.applicant__select').each(function(i, e) {
		var val = $(e).val();
		if (!val) {
			$(e).addClass('applicant__select_empty');
		}
	});

	$('.applicant').delegate('.applicant__select', 'change', function() {
		if ($(this).val().length) {
			$(this).removeClass('applicant__select_empty');
		} else {
			$(this).addClass('applicant__select_empty');
		}
	});

	$('.applicant').delegate('.month', 'change', function(e) {
		var month = $(this).val();
		var day;
		var year;
		if (month !== '') {
			if (month == 3 || month == 5 || month == 8 || month == 10) {
				day = $(this).prev().val();
				if (day) {
					if (day > 30) {
						$(this).prev().val('');
					}
				}

				if ($(this).prev()) {
					$(this).prev().find('option').last().hide();
				}
			}

			if (month == 1) {
				day = $(this).prev().val();
				year = $(this).next().val();
				if (day && year) {
					if (year % 4 == 0 && day > 29) {
						if ($(this).prev()) {
							$(this).prev().val('');
							$(this).prev().find('option').last().hide();
							$(this).prev().find('option').last().prev().hide();
						}
					}

					if (year % 4 != 0 && day > 28) {
						if ($(this).prev()) {
							$(this).prev().val('');
							$(this).prev().find('option').last().hide();
							$(this).prev().find('option').last().prev().hide();
							$(this).prev().find('option').last().prev().prev().hide();
						}
					}
				}
			}

			if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) {
				if ($(this).prev()) {
					$(this).prev().find('option').last().show();
					$(this).prev().find('option').last().prev().show();
					$(this).prev().find('option').last().prev().prev().show();
				}
			}
		}
	});

	$('.applicant').delegate('.year', 'change', function() {
		var year = $(this).val();
		var month = $(this).prev().val();
		var day = $(this).prev().prev().val();

		if (day && month && month && month == 1) {
			if (year % 4 == 0) {
				$(this).prev().prev().find('option').last().hide();
				$(this).prev().prev().find('option').last().prev().hide();
				if (day > 29) {
					$(this).prev().prev().val('');
				}
			} else {
				$(this).prev().prev().find('option').last().hide();
				$(this).prev().prev().find('option').last().prev().hide();
				$(this).prev().prev().find('option').last().prev().prev().hide();
				if (day > 28) {
					$(this).prev().prev().val('');
				}
			}
		}
	});

	$('.applicant__checkbox_expirience').prop('checked', false);

	$('.applicant__checkbox_expirience').click(function(e) {
		if ($(this).prop('checked')) {
			$('.applicant__header_expirience, .applicant__division_expirience').stop().slideDown();
		} else {
			$('.applicant__header_expirience, .applicant__division_expirience').stop().slideUp();
			$('.applicant__header_expirience').toggleClass('applicant__header_opened');
		}
	});

	$('.applicant__button__add-expirience').click(function() {
		var exp = $('.applicant__division-inner').eq(0).clone();
		$(exp).hide();
		$(exp).insertBefore($('.applicant__line_button'));
		$('<div class="applicant__line_empty"></div>').insertBefore($('.applicant__division-inner').last());
		$(exp).slideDown();
		$(exp).find('.remove-block').show();
		$(exp).find('select').each(function(i, e) {
			$(e).val('');
			$(e).addClass('applicant__select_empty');
		});
		$(exp).find('input').each(function(i, e) {
			$(e).val('');
		});
	});

	$('.education_add-more').click(function() {
		var exp = $('.education__block').eq(0).clone();
		$(exp).hide();
		$(exp).insertBefore($('.applicant__line_add-more-education'));
		$('<div class="applicant__line_empty"></div>').insertBefore($('.education__block').last());
		$(exp).slideDown();
		$(exp).find('.remove-block').show();
		$(exp).find('select').each(function(i, e) {
			$(e).val('');
			$(e).addClass('applicant__select_empty');
		});

		$(exp).find('input').each(function(i, e) {
			$(e).val('');
		});
	});

	$('.applicant__file_photo').val('');
	// Показывает загруженную фотку
	$('.applicant__file_photo').change(function(e) {
		if ($(this).val()) {
			var file = this.files[0];
			var img = $('.applicant__avatar');
			var reader = new FileReader();

			reader.onload = function() {
				$(img).attr('src', reader.result);
				$('.applicant__avatar-close').show();
			}

			if (file) {
				reader.readAsDataURL(file);
			} else {
				$(img).attr('src', '');
				$('.applicant__avatar-close').hide();
			}
		}
	});

	$('.applicant__file_cv').change(function() {
		if ($(this).val()) {
			console.log(this.files[0].name)
			$('.applicant__cv-filename').html(this.files[0].name);
		} else {
			$('.applicant__cv-filename').empty();
		}
	})

	// Удаление фотографии
	$('.applicant__avatar-close').click(function() {
		$('.applicant__avatar').removeAttr('src');
		$('.applicant__file_photo').val('');
		$('.applicant__avatar-close').hide();
	});

	$('.applicant').delegate('.remove-block', 'click', function() {
		var _this = this;
		$(_this).parent().prev().fadeOut({queue: false})
		$(_this).parent().fadeOut(function() {
			$(_this).parent().remove()
		})
	});

	$('.applicant__header').click(function() {
		$(this).next().stop().slideToggle();
		$(this).toggleClass('applicant__header_opened');
	}).mousedown(function(e) {
		e.preventDefault();
	});

	$('.button__form-cv').click(function() {
		$(this).prop('disabled', true);
		var photo = $('.applicant__file_photo').prop('files')[0];
		var personal = getPersonalInfo();
		var education = getEducation();
		var experience = getExperience();
		var otherInfo = getOtherInfo();
		var fd = new FormData();
		fd.append('personal', personal);
		fd.append('education', education);
		fd.append('otherInfo', otherInfo);
		fd.append('mode', 'form');

		if (photo) {
			fd.append('photo', photo);
		}

		if (experience) {
			fd.append('experience', experience);
		}

		$.ajax({
			url: 'cv.php',
			type: 'POST',
			data: fd,
			processData: false,
			contentType: false,
			success: function(data) {
				$('.button__form-cv').prop('disabled', false);
				sendRes(data);
			},
			error: function() {
				$('.button__form-cv').prop('disabled', false);
				alert('Не удалось связаться с сервером');
			}
		});
	});

	$('.applicant__button_ready-cv').click(function() {
		$(this).prop('disabled', true);
		var fd = new FormData();
		var cv = $('.applicant__file_cv').prop('files')[0];
		fd.append('mode', 'cv');
		fd.append('cv', cv);

		$.ajax({
			url: 'cv.php',
			type: 'POST',
			data: fd,
			contentType: false,
			processData: false,
			success: function(data) {
				$('.applicant__button_ready-cv').prop('disabled', false);
				sendRes(data);
			},
			error: function() {
				$('.applicant__button_ready-cv').prop('disabled', false);
				alert('Не удалось связаться с сервером');
			}
		});
	});

	$('.callback__button').click(function() {
		var data = {};
		data.fname = $('.callback__fname').val();
		data.name = $('.callback__name').val();
		data.email = $('.callback__email').val();
		data.phone = $('.callback__phone').val();
		data.message = $('.callback__comment').val();

		$('.callback__button').prop('disabled', true);

		$.ajax({
			url: 'callback.php',
			type: 'POST',
			data: data,
			success: function(data) {
				if (data) {
					alert('Сообщение отправлено');
				} else {
					alert('Не удалось отправить сообщение');
				}
				$('.callback__button').prop('disabled', false);
			},
			error: function() {
				alert('Не удалось отправить сообщение');
				$('.callback__button').prop('disabled', false);
			}
		});
	});

	function sendRes(data) {
		if (data == 1) {
			alert('Ваше резюме отправлено');
		} else {
			alert('Не удалось отправить резюме');
		}
	}

	function getPersonalInfo() {
		var personal = {
			sname: $('.applicant__input_sname').val(),
			name: $('.applicant__input_name').val(),
			dayOfBirth: $('.applicant__day-of-birth').val(),
			monthOfBirth: $('.applicant__month-of-birth').val(),
			yearOfBirth: $('.applicant__year-of-birth').val(),
			sex: $('input[name=sex]:checked').val(),
			living: $('.applicant__input_city').val(),
			citizen: $('.applicant__input_citizen').val(),
			email: $('.applicant__input_email').val(),
			phone: $('.applicant__input_phone').val(),
			aim: $('.applicant__textarea_aim').val(),
			haveExperience: $('.applicant__checkbox_expirience').prop('checked')
		};

		return JSON.stringify(personal);
	}

	function getEducation() {
		var ed = [];
		var tmp;

		$('.education__block').each(function(i, e) {
			tmp = {
				startYear: $(e).find('.education__start-year').val(),
				endYear: $(e).find('.education__end-year').val(),
				school: $(e).find('.education__school').val(),
				speciality: $(e).find('.education__spec').val(),
				department: $(e).find('.education__dep').val()
			};
			ed.push(tmp);
		});

		return JSON.stringify(ed);
	}

	function getExperience() {
		var ex = [];
		var tmp;

		if (!$('.applicant__checkbox_expirience').prop('checked')) {
			return false;
		}

		$('.applicant__division-inner').each(function(i, e) {
			tmp = {
				startMonth: $(e).find('.expirience__start-month').val(),
				startYear: $(e).find('.expirience__start-year').val(),
				endMonth: $(e).find('.expirience__end-month').val(),
				endYear: $(e).find('.expirience__end-year').val(),
				companyName: $(e).find('.applicant__input_company-name').val(),
				duties: $(e).find('.applicant__textarea_duties').val(),
				projects: $(e).find('.applicant__textarea_projects').val()
			};
			ex.push(tmp);
		});

		return JSON.stringify(ex);
	}

	function getOtherInfo() {
		var info = {
			addEducation: $('.applicant__textarea_add-ed').val(),
			languages: $('.applicant__textarea_langs').val(),
			compHabits: $('.applicant__textarea_comp').val(),
			addInfo: $('.applicant__textarea_inf').val(),
			personalQualities: $('.applicant__textarea_personal-q').val(),
			payment: $('.applicant__input_payment').val()
		};

		return JSON.stringify(info);
	}

	$('input[name=cvmode]').change(function() {
		var val = $(this).val();
		
		if (val == 'form') {
			$('.cv-mode_form').stop().slideDown({queue: false});
			$('.cv-mode_cv').stop().slideUp({queue: false});
		} else {
			$('.cv-mode_form').stop().slideUp({queue: false});
			$('.cv-mode_cv').stop().slideDown({queue: false});
		}
	});

	$('.services__type').click(function() {
		var h = $(this).find('span').text();
		var d = $(this).find('div').text();
		var data = $(this).attr('data');
		var position = $(this).position().top + 30;

		$('.modal__header').html(h).removeAttr('class').addClass('modal__header modal__header_' + data);
		$('.modal__text').html(d);
		$('.services__modal').css('top', position).fadeIn();
	});

	$('.services__modal-close').click(function() {
		$('.services__modal').fadeOut();
	});

	$('input[name=cvmode]').click(function() {
		var val = $(this).val();
		var offsetTop = $('.cv-mode_form').offset().top || $('.cv-mode_cv').offset().top;

		$('html, body').animate({
			scrollTop: offsetTop - 25
		});
	});
})(jQuery);